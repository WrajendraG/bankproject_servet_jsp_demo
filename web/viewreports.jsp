<%-- 
    Document   : viewreports
    Created on : 24 Jun, 2017, 9:08:49 AM
    Author     : rajendra
--%>
<%@page import="java.util.ArrayList,javaClasses.Transation"%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%

String username = session.getAttribute("name").toString();
String accountnumber = session.getAttribute("accno").toString();
ArrayList a1= (ArrayList)request.getAttribute("list");

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>mini statement</title>

</head>

<body style="background-color:black" method="post">
<table width="900" border="1" align="center" cellpadding="0" cellspacing="0" style="font-weight:normal; background-color:#FFFFFF">
  <tr>
      <th colspan="3" scope="col" style="height:90px; background-color:#2175bc;"> WelCome <font color="black"><%= username %></font>
  </tr>
  <tr>
      <td colspan="3">&nbsp;<% out.println(new java.util.Date()); %> <b>Account Operations</b> </td>
  </tr>
  <tr>
<td width="200" >
    <div class="menutitle">
      Account No: &nbsp;<%= accountnumber %>
  <ul>
         <li><a href="Profile.jsp">Home Page</a></li>
      <li><a href="Deposit.jsp">Deposit Amount</a></li>
    <li><a href="withdraw.jsp">Withdraw Amount</a></li>
    <li><a href="./GB">Balance Inquire</a></li>
    <li><a href="transfer.jsp">Transfer Amount</a></li>
    <li><a href="./MS">Mini Statement</a></li>
    <li> <a href="./LOS">LogOut</a></li>
  </ul>
  <div class="menutitle">&nbsp;</div>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>	</td>
    <td colspan="2" style="padding:20px;">
	<div class="box1">
	<marquee><h2><font color="#FF0000">  ${mini} </font></h2></marquee>
	</div>
	<br/>
	<form  method="post" action="./MS">
	  <table width="96%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <th colspan="5" bgcolor="#333333" scope="col"><font color="#FFFFFF">Following are the Mini Statement of Last 5 Transition of Your Account(s). </font></th>
        </tr>
        <tr>
          <td colspan="5">&nbsp;</td>
          </tr>
       <tr>
          <td bgcolor="#2175BC"><div align="center" class="style2">A/c No </div></td>
          <td bgcolor="#2175BC"><div align="center" class="style2">Amount</div></td>
          <td bgcolor="#2175BC"><div align="center" class="style2">Operation</div></td>
         <td bgcolor="#2175BC"><div align="center" class="style2">Date - Time </div></td>
         <td bgcolor="#2175BC"><div align="center" class="style2">Remark</div></td>
        </tr>
              <%
		for(int i=0;i<a1.size();i++) 
                {
                    Transation t2 = (Transation)a1.get(i);
		 
		%>
        <tr>
          <td><div align="center"><%out.print(t2.getAccountnumber());%></div></td>
          <td><div align="center"><%out.print(t2.getAmount());%></div></td>
          <td><div align="center"><%out.print(t2.getType());%></div></td>
          <td><div align="center"><%out.print(t2.getDate());%></div></td>
          <td><div align="center"><%out.print(t2.getRemark());%></div></td>
        </tr>
		<% } %>
             
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="14%">&nbsp;</td>
          <td width="14%">&nbsp;</td>
          <td width="15%">&nbsp;</td>
          <td width="16%">&nbsp;</td>
          <td width="41%">&nbsp;</td>
        </tr>
      </table>
      </form>
	</td>
  </tr>
  </tr>
  <tr style="height:30px;">
    <td colspan="3" style="background-color:#2175bc;">&nbsp;</td>
  </tr>
</table>
</body>

</html>