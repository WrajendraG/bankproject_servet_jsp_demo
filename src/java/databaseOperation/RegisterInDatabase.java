
package databaseOperation;

import java.sql.*;
import java.util.ArrayList;
import javaClasses.CommonPojo;
import javaClasses.Transation;
/**
 *
 * @author rajendra
 */
public class RegisterInDatabase 
{
    CommonPojo registerUsingPojo = new CommonPojo();
   
    
     public static Connection prepareConn()
        {
		Connection c=null;
		try
                {
	    	Class.forName("com.mysql.jdbc.Driver");
	    	c = DriverManager.getConnection("jdbc:mysql://localhost:3306/mybankproject","root","root");
		}
		catch (Exception exception) 
                {
			System.err.println (exception);
		}
		return c;
	}//prepareConn
	
	public static void closeConn(Connection c)
        {
		try 
                {
			if(c!=null)
				c.close();    
		}
		catch (Exception exception) 
                {
			System.err.println (exception);
		}
	}//closeConn

    //----------------------------------------------------------------------------------------------
    
    public static int registerHere(CommonPojo registerUsingPojo )
    { 
        
     java.util.Random r = new java.util.Random();
     int accountnumber = r.nextInt(1000);
   
         
    
    String username = registerUsingPojo.getusername();
    String password = registerUsingPojo.getpassword();
    String question = registerUsingPojo.getquestion();
    String answer = registerUsingPojo.getanswer();
    String address = registerUsingPojo.getaddress();
    String email = registerUsingPojo.getemail();
    String mobile = registerUsingPojo.getmobile();
    
    String sql ="INSERT INTO mybankproject.register (accountnumber, username, password, question, answer, address, email, mobile) VALUES ('"+accountnumber +"','"+username +"','"+password +"','"+question +"','"+answer +"','"+address +"','"+email +"','"+mobile +"');";
    
  //  String sql2="INSERT INTO mybankproject.transaction() ";
            int update =0;
            Connection c = null;
            try
            {
               c = prepareConn();
               Statement st = c.createStatement();
               update =   st.executeUpdate(sql);
               
            }catch(Exception exception)
            {
                System.err.println(exception);
            }
               closeConn(c);
            return update;
    }
    public static CommonPojo checkuserandPass(CommonPojo registerUsingPojo)
    {
        String email = registerUsingPojo.getemail();
        String password = registerUsingPojo.getpassword();
         
        String sql ="SELECT accountnumber,username FROM mybankproject.register WHERE email ='"+email+"' AND password = '"+password+"'";
                      
                CommonPojo cp= new CommonPojo();
                boolean check = false;
		Connection c=null;
		try 
                {
			c = prepareConn();
			Statement st = c.createStatement();
			ResultSet rs = st.executeQuery(sql);
			if(rs.next())
                        {

                               check = true;
                               cp.setAccountnumber(rs.getInt(1));
                               cp.setusername(rs.getString(2));
                               
                             
             		}
                      
                                  
		}
		catch (Exception ex) 
                {
			System.out.println (ex);
		}
		closeConn(c);
		return cp;
      
    }
    public static boolean checkemailandmobile(CommonPojo registerUsingPojo)
    {
        String email = registerUsingPojo.getemail();
        String mobile = registerUsingPojo.getmobile();
        
        String sql ="SELECT accountnumber,username FROM mybankproject.register WHERE email ='"+email+"' AND mobile = '"+mobile+"'";
        
        //boolean result = CommonMethodes.checkUser(sql);
        
        return true;
    }

    public static int Deposit(Transation p)
    {
        int accountnumber = p.getAccountnumber();
        float amount = p.getAmount();
        String remark = p.getRemark();

        String sql ="INSERT INTO mybankproject.txn (accountnumber,amount,type,tdate,remark) VALUES ("+accountnumber+","+amount+", 'Deposit', current_time(), '"+remark+"')";
        
        //insert into mybankproject.txn(accountnumber,amount, type, tdate, remark) values(1, 9000,'deposit', current_time(), 'na');
        int update =0;
            Connection c = null;
            try
            {
               c = prepareConn();
               Statement st = c.createStatement();
               update =   st.executeUpdate(sql);
               
            }catch(Exception exception)
            {
                System.err.println(exception);
            }
               closeConn(c);
           return update;
 
    }
    public static int transferDeposit(Transation t1)
    {
        int accountnumber = t1.getTransferaccountnumber();
        float amount = t1.getTransferamount();
        String remark = t1.getRemark();

        String sql ="INSERT INTO mybankproject.txn (accountnumber,amount,type,tdate,remark) VALUES ("+accountnumber+","+amount+", 'Deposit by transfer', current_time(), '"+remark+"')";
        
        //insert into mybankproject.txn(accountnumber,amount, type, tdate, remark) values(1, 9000,'deposit', current_time(), 'na');
        int update =0;
            Connection c = null;
            try
            {
               c = prepareConn();
               Statement st = c.createStatement();
               update =   st.executeUpdate(sql);
               
            }catch(Exception exception)
            {
                System.err.println(exception);
            }
               closeConn(c);
           return update;
 
    }
    
      public static int Withdraw(Transation p)
    {
        int accountnumber = p.getAccountnumber();
        float amount = p.getAmount();
        String remark = p.getRemark();

        String sql ="INSERT INTO mybankproject.txn (accountnumber,amount,type,tdate,remark) VALUES ("+accountnumber+","+amount+", 'Withdraw', current_time(), '"+remark+"')";
        
        //insert into mybankproject.txn(accountnumber,amount, type, tdate, remark) values(1, 9000,'deposit', current_time(), 'na');
        int update =0;
            Connection c = null;
            try
            {
               c = prepareConn();
               Statement st = c.createStatement();
               update =   st.executeUpdate(sql);
               
            }catch(Exception exception)
            {
                System.err.println(exception);
            }
               closeConn(c);
               
           return update;
 
    }
      
      public static int transferWithdraw(Transation t)
    {
        int accountnumber = t.getAccountnumber();
        float amount = t.getAmount();
        String remark = t.getRemark();

        String sql ="INSERT INTO mybankproject.txn (accountnumber,amount,type,tdate,remark) VALUES ("+accountnumber+","+amount+", 'Withdraw by transfer', current_time(), '"+remark+"')";
        
        //insert into mybankproject.txn(accountnumber,amount, type, tdate, remark) values(1, 9000,'deposit', current_time(), 'na');
        int update =0;
            Connection c = null;
            try
            {
               c = prepareConn();
               Statement st = c.createStatement();
               update =   st.executeUpdate(sql);
               
            }catch(Exception exception)
            {
                System.err.println(exception);
            }
               closeConn(c);
               
           return update;
 
    }
       
    
    
    public static int insertPassword( CommonPojo registerUsingPojo)
    {
        int update=0;
            
        int accountnumber=registerUsingPojo.getAccountnumber();
        String password= registerUsingPojo.getpassword();
        
      String sql="UPDATE mybankproject.register SET password ="+password+" WHERE `accountnumber`="+accountnumber+"";
        
      Connection c=null;
      try
      {
          c=prepareConn();
          Statement s = c.createStatement();
          update=s.executeUpdate(sql);
      
      }catch(Exception e)
      {
      System.err.print(e);
      }
              closeConn(c);
        return update;
    }
    
    public static CommonPojo getBalance(CommonPojo p)
    {
         int accountnumber=p.getAccountnumber();
         String username=p.getusername();
         
      
        CommonPojo p1=new CommonPojo();
      
      String sql="select amount from mybankproject.txn where accountnumber="+accountnumber+" order by tid desc limit 1";
        
      Connection c=null;
      try
      {
          c=prepareConn();
          Statement s = c.createStatement();
         ResultSet rs=s.executeQuery(sql);
          while (rs.next()) 
          {
             p1.setAmount(rs.getFloat(1));
              
          }
      
      }catch(Exception e)
      {
      System.err.print(e);
      }
              closeConn(c);
              return p1;
    
    
    }
    public static Transation transferaccountgetBalance(Transation t)
    {
         int accountnumber=t.getTransferaccountnumber();
   
        Transation t1=new Transation();
      
      String sql="select amount from mybankproject.txn where accountnumber="+accountnumber+" order by tid desc limit 1";
        
      Connection c=null;
      try
      {
          c=prepareConn();
          Statement s = c.createStatement();
         ResultSet rs=s.executeQuery(sql);
          while (rs.next()) 
          {
             t1.setAmount(rs.getFloat(1));
              
          }
      
      }catch(Exception e)
      {
      System.err.print(e);
      }
              closeConn(c);
              return t1;
    
    
    }
    public static ArrayList getAccountNoByName()
            
    {
        //SELECT acc_no, acc_type FROM acc_details
        String sql="SELECT accountnumber,username from mybankproject.register;";
		ArrayList a = new ArrayList();
		Connection c=null;
		try {
			c = prepareConn();
			Statement st = c.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next())
                        {
				a.add(rs.getInt(1)+"");
                                a.add(rs.getString(2)+"");
                            
                       
                           
			}
		}
		catch (Exception ex) 
                {
			System.out.println (ex);
		}
		closeConn(c);
		return a;
	}
    
    
    public static ArrayList miniStatement (Transation t)
    {
        int accountnumber=t.getAccountnumber();
        
        String sql = "Select accountnumber,amount,type,tdate,remark from mybankproject.txn where accountnumber="+accountnumber+" order by tid desc limit 5";
        
        ArrayList<Transation> al = new ArrayList<Transation>();
		Connection c=null;
		try {
			c = prepareConn();
			Statement st = c.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next())
                        {
                            Transation t1= new Transation();
				
				t1.setAccountnumber(rs.getInt(1));
				t1.setAmount(rs.getFloat(2));
				t1.setType(rs.getString(3));
				t1.setDate(rs.getString(4)); // DateTime 
				t1.setRemark(rs.getString(5));
				al.add(t1);
			}
		}
		catch (Exception ex) 
                {
			System.out.println (ex);
		}
		closeConn(c);
		return al;
        
    

    }
    
    
    

    
}
  
    
    
    

