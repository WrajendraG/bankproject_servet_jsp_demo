/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package javaClasses;

/**
 *
 * @author rajendra
 */
public class CommonPojo
{
       private   String username = null;
       private   String password = null;
       private   String question = null;
       private   String answer = null;
       private   String address = null;
       private   String email = null;
       private   String mobile = null;
        
       private int accountnumber=0;
       
       private float amount=0;

    public void setAmount(float amount)
    {
        this.amount = amount;
    }

    public float getAmount() {
        return amount;
    }

    public void setAccountnumber(int accountnumber)
    {
        this.accountnumber = accountnumber;
    }

    public int getAccountnumber() 
    {
        return accountnumber;
    }
  //------------------------------------------------------------------------      
        public void setusername(String username)
        {
            this.username=username;
          
        }
        public String getusername()
        {
            return username;
        }
   //------------------------------------------------------------------------   
          
        public void setpassword(String password)
        {
            this.password=password;
          
        }
        public String getpassword()
        {
            return password;
        }
   //------------------------------------------------------------------------     
       
        public void setquestion(String question)
        {
            this.question=question;
          
        }
        public String getquestion()
        {
            return question;
        }
   //------------------------------------------------------------------------     
          
        public void setanswer(String answer)
        {
            this.answer=answer;
          
        }
        public String getanswer()
        {
            return answer;
        }
   //------------------------------------------------------------------------     
     
        public void setaddress(String address)
        {
            this.address=address;
          
        }
        public String getaddress()
        {
            return address;
        }
   //------------------------------------------------------------------------     
           
        public void setemail(String email)
        {
            this.email=email;
          
        }
        public String getemail()
        {
            return email;
        }
   //------------------------------------------------------------------------     
         
        public void setmobile(String mobile)
        {
            this.mobile=mobile;
          
        }
        public String getmobile()
        {
            return mobile;
        }
   //------------------------------------------------------------------------     
    
    
}
