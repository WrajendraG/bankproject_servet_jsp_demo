/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaClasses;


public class Transation 
{
    
    private int accountnumber=0;
    private float amount=0;
    private String type=null;
    private String date=null;
    private String remark=null;
    private int transferaccountnumber=0;
    private float transferamount=0;

    public void setTransferamount(float transferamount) {
        this.transferamount = transferamount;
    }

    public float getTransferamount() {
        return transferamount;
    }

    public void setTransferaccountnumber(int transferaccountnumber) 
    {
        this.transferaccountnumber = transferaccountnumber;
    }

    public int getTransferaccountnumber() {
        return transferaccountnumber;
    }

    public void setAccountnumber(int accountnumber) {
        this.accountnumber = accountnumber;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getAccountnumber() {
        return accountnumber;
    }

    public float getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

    public String getDate() {
        return date;
    }

    public String getRemark() {
        return remark;
    }
    
    
    
}
