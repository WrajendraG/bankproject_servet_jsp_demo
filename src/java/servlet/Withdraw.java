
package servlet;

import databaseOperation.RegisterInDatabase;
import java.io.IOException;
import java.io.PrintWriter;
import javaClasses.CommonPojo;
import javaClasses.Transation;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Withdraw extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter())
        {
            
            float  totalamount=0;
            
             int accountnumber = Integer.parseInt(request.getParameter("accountnumber"));
             float amount= Float.parseFloat(request.getParameter("amount"));
             String remark = request.getParameter("remark");
             String username=request.getParameter("username");
            
                CommonPojo p =new CommonPojo();             
                p.setAccountnumber(accountnumber);
                p.setusername(username);

               CommonPojo result = RegisterInDatabase.getBalance(p);
            
               float bal= result.getAmount();
        
                System.out.println("Current Balance : "+bal);
           if(bal == 0)
           {
	
        
	      request.setAttribute("notwithdraw", "<b>You Can't Withdraw Current Amount is zero.(Deposit amount)</b>");
                
                request.getRequestDispatcher("withdraw.jsp").forward(request, response);
                
            }
            else if((bal- amount)<1000)
           {
	
        
	      request.setAttribute("notwithdraw", "<b>You Can't Withdraw Amount you Cross minimun balance limit  </b>");
                
                request.getRequestDispatcher("withdraw.jsp").forward(request, response);
                
            }
            else if(bal == amount)
           {
	
        
	      request.setAttribute("notwithdraw", "<b>You Can't Withdraw Total Amount Minimally 1000 Balance is Requried </b>");
                
                request.getRequestDispatcher("withdraw.jsp").forward(request, response);
                
            }
            else if(bal < amount)
            {

               request.setAttribute("notwithdraw", "<b>You Can't Withdraw Accessing Amount is Geter then Current Amount</b>");

                request.getRequestDispatcher("withdraw.jsp").forward(request, response);
            }
            else if(bal <= 1000)
            {       
                request.setAttribute("notwithdraw", "<b>You Can't Withdraw Amount less then  1000 </b>");

                request.getRequestDispatcher("withdraw.jsp").forward(request, response);
            }
            else 
            {
                          totalamount = bal - amount;
                                  System.out.println("After Withraw Balance : "+totalamount);
                          Transation t =new Transation();

                          t.setAccountnumber(accountnumber);
                          t.setAmount(totalamount);
                          t.setRemark(remark);

                          int result2 =  RegisterInDatabase.Withdraw(t);

                            if(result2 > 0)
                            {
                                request.setAttribute("notwithdraw", "<b>Amount Successfully withdraw<b>");

                                request.getRequestDispatcher("withdraw.jsp").forward(request, response);

                            }
                            else
                            {
                                request.setAttribute("notwithdraw", "Amount Does Not withdraw Try Again ");

                                request.getRequestDispatcher("withdraw.jsp").forward(request, response);
                            }

                            }

                }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
