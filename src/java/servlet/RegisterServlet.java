
package servlet;
import javaClasses.CommonPojo;
import databaseOperation.*;
//--------------------------above User Defined Package
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class RegisterServlet extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter())
        {
           String username = request.getParameter("username");
           String password = request.getParameter("password");
           String question = request.getParameter("question");
           String answer = request.getParameter("answer");
           String address = request.getParameter("address");
           String email = request.getParameter("email");
           String mobile = request.getParameter("mobile");
           
           
           CommonPojo registerUsingPojo = new CommonPojo();
           
           registerUsingPojo.setusername(username);
           registerUsingPojo.setpassword(password);
           registerUsingPojo.setquestion(question);
           registerUsingPojo.setanswer(answer);
           registerUsingPojo.setaddress(address);
           registerUsingPojo.setemail(email);
           registerUsingPojo.setmobile(mobile);
           
          
            int result = RegisterInDatabase.registerHere(registerUsingPojo);
           
          if(result == 1)
          {
              request.setAttribute("message", "<b>Registered Successfully</b> <a href='index.jsp'>Login Here</a>");
              request.getRequestDispatcher("/Register.jsp").forward(request, response);
         
          }
          else
          {
              request.setAttribute("message", " <b>Not Registered Successfully Do Again<b>");
              request.getRequestDispatcher("/Register.jsp").forward(request, response);
          }
            
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
