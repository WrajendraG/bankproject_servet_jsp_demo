
package servlet;

import databaseOperation.RegisterInDatabase;
import java.io.IOException;
import java.io.PrintWriter;
import javaClasses.CommonPojo;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SetPassword extends HttpServlet
{

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter())
        {
        
            
         
            String username = request.getParameter("username");
            int accountnumber = Integer.parseInt("accountnumber");
            String passward = request.getParameter("passward");
            
               CommonPojo registerUsingPojo = new CommonPojo();
        
               registerUsingPojo.setAccountnumber(accountnumber);
               registerUsingPojo.setusername(username);
               registerUsingPojo.setpassword(passward);

               int result = RegisterInDatabase.insertPassword(registerUsingPojo);
              
          if(result > 0)
          {
             
              
              request.setAttribute("NewVerfication", "<b>New Password Generated Successfully<a href='index.jsp'>Login Here</a></b>");
              request.getRequestDispatcher("/NewPassword.jsp").forward(request, response);
         
          }
          else
          {
              request.setAttribute("NewVerfication", " <b>Password Not Generated <b>");
              request.getRequestDispatcher("/NewPassword.jsp").forward(request, response);
          }
               
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
