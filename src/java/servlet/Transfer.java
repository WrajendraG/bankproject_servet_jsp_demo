
package servlet;

import databaseOperation.RegisterInDatabase;
import java.io.IOException;
import java.io.PrintWriter;
import javaClasses.CommonPojo;
import javaClasses.Transation;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Transfer extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
       float  totalamount=0;
            
                  String username=request.getParameter("username");
             int accountnumber = Integer.parseInt(request.getParameter("accountnumber"));           
             int transferaccountnumber = Integer.parseInt(request.getParameter("ta"));
             float transferamount= Float.parseFloat(request.getParameter("amount"));
             String remark = request.getParameter("remark");
  
            
                CommonPojo p =new CommonPojo();             
                p.setAccountnumber(accountnumber);
                p.setusername(username);

               CommonPojo result = RegisterInDatabase.getBalance(p);
            
               float bal= result.getAmount();
        
               
           if(bal == 0)
           {
	
        
	      request.setAttribute("t", "<b>You Can't transfer Current Amount is zero.(Deposit amount)</b>");
                
                request.getRequestDispatcher("transfer.jsp").forward(request, response);
                
            }
            else if(bal == transferamount)
           {
	
        //     1200  1100  done
	      request.setAttribute("t", "<b>You Can't transfer Total Amount Minimally 1000 Balance is Requried </b>");
                
                request.getRequestDispatcher("transfer.jsp").forward(request, response);
                
            }
            else if(bal < transferamount)
            {

               request.setAttribute("t", "<b>You Can't transfer Accessing Amount is Geter then Current Amount</b>");

                request.getRequestDispatcher("transfer.jsp").forward(request, response);
            }
            else if(bal <= 1000)
            {       
                request.setAttribute("t", "<b>You Can't transfer Amount less then  1000 </b>");

                request.getRequestDispatcher("transfer.jsp").forward(request, response);
            }
            else 
            {
                          totalamount = bal - transferamount;
                                  
                          
                          Transation t =new Transation();

                          t.setAccountnumber(accountnumber);
                          t.setAmount(totalamount);
                          t.setRemark(remark);
                          t.setTransferaccountnumber(transferaccountnumber);
                          t.setTransferamount(transferamount);
                      
                           int result2 =  RegisterInDatabase.transferWithdraw(t);
                           
//---------------------------------------Before tran. check current bal.and Withraw
    
                           Transation result1 = RegisterInDatabase.transferaccountgetBalance(t);
                                
                                float result3= result1.getAmount();
                                System.out.println("First amount in tr a "+result3);

                                float total2= (transferamount+result3);

                                System.out.print("total depppp to tr a"+total2);
                               
                                Transation t1 =new Transation();

                                 //t1.setAccountnumber(accountnumber);
                                 t1.setTransferaccountnumber(transferaccountnumber);
                                 t1.setTransferamount(total2);
                                 
                                 t1.setRemark(remark);

//---------------------------------------then first check current bal from tran. a/c then both(current bal+ Enteramount) deposit 

                                  int result4 =  RegisterInDatabase.transferDeposit(t1);

                                    if(result4 >0)
                                    {
                                        request.setAttribute("t", "<b>Amount Successfully transfer<b>");

                                        request.getRequestDispatcher("transfer.jsp").forward(request, response);

                                    }
                                    else
                                    {
                                       request.setAttribute("t", "Amount Does Not transfer Try Again ");

                                     request.getRequestDispatcher("transfer.jsp").forward(request, response);
                                    }
                           

                }
   

        }

    }

    

  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
   {
        processRequest(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
     {
        processRequest(request, response);
    }

} 

