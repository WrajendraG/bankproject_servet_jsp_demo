
package servlet;

import databaseOperation.RegisterInDatabase;
import javaClasses.CommonPojo;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class Login extends HttpServlet 
{

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
       {
        response.setContentType("text/html;ch1arset=UTF-8");
        try (PrintWriter out = response.getWriter())
        {
            
            String email =request.getParameter("u");
            String password =request.getParameter("p");
            
           CommonPojo registerUsingPojo = new CommonPojo();
           
           registerUsingPojo.setemail(email);
           registerUsingPojo.setpassword(password);
           
    
             CommonPojo result = RegisterInDatabase.checkuserandPass(registerUsingPojo);
           
           if(result!=null)
          {
              HttpSession session=request.getSession();
          
                 session.setAttribute("accno",result.getAccountnumber());
                 session.setAttribute("name", result.getusername());
               
      
                           
              
              request.setAttribute("verfication", "<b>Varification is Successfully</b>");
              request.getRequestDispatcher("/Profile.jsp").forward(request,response);
         
          }
          else
          {
              request.setAttribute("verfication1", " <b>Enter Valid Username and PassWord <b>");
              request.getRequestDispatcher("/index.jsp").forward(request, response);
          }
            
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
