package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Profile_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write('\n');



String Username = (String)session.getAttribute("sessionusername");



      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
      out.write("<head>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n");
      out.write("<title>Home Pages Bank</title>\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body style=\"background-color:aqua\">\n");
      out.write("<table width=\"900\" border=\"1\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-weight:normal; background-color:#FFFFFF\">\n");
      out.write("  <tr>\n");
      out.write("    <th colspan=\"3\" scope=\"col\" style=\"height:90px; background-color:#2175bc;\">>\n");
      out.write("      \n");
      out.write("      \n");
      out.write("  </tr>\n");
      out.write("  <tr>\n");
      out.write("    <td colspan=\"3\">&nbsp;</td>\n");
      out.write("  </tr>\n");
      out.write("  <tr>\n");
      out.write("<td width=\"160\" >\n");
      out.write("<div id=\"ddblueblockmenu\">\n");
      out.write("  <div class=\"menutitle\">Account Operations</div>\n");
      out.write("  <ul>\n");
      out.write("    <li><a href=\"main.jsp\">Welcome,&nbsp;");
      out.print( Username );
      out.write("</a></li>\n");
      out.write("    <li><a href=\"account.jsp\">Create Account</a></li>\n");
      out.write("\t<li><a href=\"deposite.jsp\">Deposit</a></li>\n");
      out.write("    <li><a href=\"withdraw.jsp\">Do Withdraw</a></li>\n");
      out.write("    <li><a href=\"get-balance.jsp\">Get Balance</a></li>\n");
      out.write("\t<li><a href=\"transfer.jsp\">Transfer Amount</a></li>\n");
      out.write("\t<li><a href=\"viewreports.jsp\">View Report</a></li>\n");
      out.write("\t<li><a href=\"logOff.jsp\">LogOut</a></li>\n");
      out.write("  </ul>\n");
      out.write("  <div class=\"menutitle\">&nbsp;</div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<p>&nbsp;</p>\n");
      out.write("<p>&nbsp;</p>\n");
      out.write("<p>&nbsp;</p>\n");
      out.write("<p>&nbsp;</p>\t</td>\n");
      out.write("    <td colspan=\"2\" style=\"padding:20px;\">\n");
      out.write("\t<div class=\"box1\">\n");
      out.write("\t<marquee><h2><font color=\"#FF0000\">Welcome to online banking</font></h2></marquee>\n");
      out.write("\t</div>\n");
      out.write("\t<br/><p align=\"left\" style=\"line-height:18px; padding:10px; font-weight:normal\">The central concept of the application is to allow the  customer(s) to service virtually using the Internet with out going to bank and  allow customers to open new account, withdraw, deposit,  close and  getting balance using this banking service.&nbsp;  The information pertaining to the customers stores on an RDBMS at the  server side (BANK).&nbsp; The Bank services  the customers according to the customer&rsquo;s intention and it updates and backups  of each customer transaction accordingly.</p>\n");
      out.write("\t</td>\n");
      out.write("  </tr>\n");
      out.write("  <tr style=\"height:30px;\">\n");
      out.write("    <td colspan=\"3\" style=\"background-color:#2175bc;\">&nbsp;</td>\n");
      out.write("  </tr>\n");
      out.write("</table>\n");
      out.write("</body>\n");
      out.write("\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
