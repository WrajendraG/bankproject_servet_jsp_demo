package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;

public final class Register_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=iso-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
      out.write("<head>\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n");
      out.write("<title>STATE BANK OF INDIA</title>\n");
      out.write("<link href=\"css/menu.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("<link href=\"css/main.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
      out.write("<style type=\"text/css\">\n");
      out.write("<!--\n");
      out.write("html,body{\n");
      out.write("    background-image: url(images/img.gif);\n");
      out.write("}\n");
      out.write("</style>\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("<table width=\"900\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-weight:normal; background-color:#FFFFFF\">\n");
      out.write("  <tr>\n");
      out.write("    <th colspan=\"3\" scope=\"col\" style=\"height:90px; background-color:#2175bc;\"><object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0\" width=\"900\" height=\"90\">\n");
      out.write("      <param name=\"movie\" value=\"images/banks.swf\" />\n");
      out.write("      <param name=\"quality\" value=\"high\" />\n");
      out.write("      <embed src=\"images/banks.swf\" quality=\"high\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" type=\"application/x-shockwave-flash\" width=\"900\" height=\"90\"></embed>\n");
      out.write("    </object></th>\n");
      out.write("  </tr>\n");
      out.write("  <tr>\n");
      out.write("    <td colspan=\"3\">&nbsp;</td>\n");
      out.write("  </tr>\n");
      out.write("  <tr>\n");
      out.write("<td width=\"160\" ><p>&nbsp;</p>\n");
      out.write("<p>&nbsp;</p>\n");
      out.write("<p>&nbsp;</p>\n");
      out.write("<p>&nbsp;</p>\t</td>\n");
      out.write("    <td colspan=\"2\"><table width=\"98%\" border=\"0\" align=\"center\" cellpadding=\"1\" cellspacing=\"0\">\n");
      out.write("      <tr>\n");
      out.write("        <th colspan=\"2\" scope=\"col\">\n");
      out.write("\t\t<div class=\"box1\">\n");
      out.write("\t<marquee><h2><font color=\"#FF0000\">STATE BANK OF INDIA</font></h2></marquee>\n");
      out.write("\t</div>\n");
      out.write("\t<br/>\n");
      out.write("\t<br/>\n");
      out.write("\t\t</th>\n");
      out.write("      </tr>\n");
      out.write("      <tr>\n");
      out.write("\t  \n");
      out.write("        <th colspan=\"2\" scope=\"col\"><form id=\"form1\" name=\"form1\" method=\"post\" action=\"doRegister.jsp\">\n");
      out.write("          <table width=\"80%\" border=\"0\" align=\"center\" cellpadding=\"2\" cellspacing=\"2\" style=\"border:#000000 solid 2px;padding:5px;\">\n");
      out.write("            <tr>\n");
      out.write("              <th colspan=\"3\" bgcolor=\"#000000\" scope=\"col\" style=\"height:20px;\"><font color=\"#FFFFFF\">Customer Registration For new Account</font></th>\n");
      out.write("              </tr>\n");
      out.write("            <tr>\n");
      out.write("              <td width=\"28%\" rowspan=\"12\"><div align=\"center\"><img src=\"images/logo.jpg\" width=\"48\" height=\"48\" /></div></td>\n");
      out.write("              <td>&nbsp;</td>\n");
      out.write("              <td>&nbsp;</td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <td width=\"24%\"><div align=\"left\">Username:</div></td>\n");
      out.write("              <td width=\"48%\"><label>\n");
      out.write("                <div align=\"left\">\n");
      out.write("                  <input name=\"username\" type=\"text\" id=\"username\" />\n");
      out.write("                  </div>\n");
      out.write("              </label></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <td><div align=\"left\">Password:</div></td>\n");
      out.write("              <td><div align=\"left\">\n");
      out.write("                <input name=\"password\" type=\"password\" id=\"password\" />\n");
      out.write("              </div></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <td><div align=\"left\">Security Question: </div></td>\n");
      out.write("              <td><div align=\"left\">\n");
      out.write("                <select name=\"question\">\n");
      out.write("                  <option value=\"Your First School Name\">Your First School Name?</option>\n");
      out.write("                  <option value=\"Your Car Number\">Your Car Number?</option>\n");
      out.write("                  <option value=\"Your Mothers Name\">Your Mothers Name?</option>\n");
      out.write("                  <option value=\"Favorite Color\">Favorite Color?</option>\n");
      out.write("                </select>\n");
      out.write("              </div></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <td><div align=\"left\">Answer:</div></td>\n");
      out.write("              <td><div align=\"left\">\n");
      out.write("                <input name=\"answer\" type=\"text\" id=\"answer\" size=\"30\" />\n");
      out.write("              </div></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <td><div align=\"left\">Address:</div></td>\n");
      out.write("              <td><div align=\"left\">\n");
      out.write("                <textarea name=\"address\" id=\"address\"></textarea>\n");
      out.write("              </div></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <td><div align=\"left\">E-mail:</div></td>\n");
      out.write("              <td><div align=\"left\">\n");
      out.write("                <input name=\"email\" type=\"text\" id=\"email\" />\n");
      out.write("              </div></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <td><div align=\"left\">Mobile:</div></td>\n");
      out.write("              <td><div align=\"left\">\n");
      out.write("                <input name=\"mobile\" type=\"text\" id=\"mobile\" />\n");
      out.write("              </div></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <td>&nbsp;</td>\n");
      out.write("              <td>&nbsp;</td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <td><label>\n");
      out.write("                <div align=\"right\">\n");
      out.write("                  <input type=\"reset\" name=\"Submit2\" value=\"Reset\" />\n");
      out.write("                  </div>\n");
      out.write("              </label></td>\n");
      out.write("              <td colspan=\"2\"><label>\n");
      out.write("                \n");
      out.write("                    <div align=\"left\">\n");
      out.write("                      <input name=\"Submit\" type=\"submit\"  value=\"Create Account\"  />\n");
      out.write("                    </div>\n");
      out.write("                  </label></td>\n");
      out.write("              </tr>\n");
      out.write("            <tr>\n");
      out.write("              <td>&nbsp;</td>\n");
      out.write("              <td>&nbsp;</td>\n");
      out.write("              <td>&nbsp;</td>\n");
      out.write("            </tr>\n");
      out.write("          </table>\n");
      out.write("                </form>        </th>\n");
      out.write("        </tr>\n");
      out.write("      <tr>\n");
      out.write("        <td width=\"50%\">&nbsp;</td>\n");
      out.write("        <td width=\"50%\">&nbsp;</td>\n");
      out.write("      </tr>\n");
      out.write("    </table></td>\n");
      out.write("  </tr>\n");
      out.write("  <tr style=\"height:30px;\">\n");
      out.write("    <td colspan=\"3\" style=\"background-color:#2175bc;\">&nbsp;</td>\n");
      out.write("  </tr>\n");
      out.write("</table>\n");
      out.write("</body>\n");
      out.write("\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
